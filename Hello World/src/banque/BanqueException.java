package banque;

public class BanqueException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BanqueException() {
		
	}
	
	public BanqueException(String message) {
		super(message);
	}

	public BanqueException(String message, Throwable cause) {
		super(message, cause);
	}

	public BanqueException(Throwable cause) {
		super(cause);
	}
	
	
}
