package banque;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;

public class Client {
	private String nom;
	private String prenom;
	private int age;
	private int numero;
	//private ArrayList<Compte> listCompte = new ArrayList<Compte>();
	private Map<Integer, Compte> mapCompte = new Hashtable<Integer, Compte>();
	
	public Client(String nom, String prenom, int age, int numero) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.numero = numero;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public int getAge() {
		return age;
	}

	public int getNumero() {
		return numero;
	}
	
	public Map<Integer, Compte> getListCompte() {
		return mapCompte;
	}
	
	public void ajouterCompte(Compte c) throws BanqueException {
		if (mapCompte.size() < 5) {
			mapCompte.put(c.getNumero(), c);
			System.out.println("Votre nouveau compte a été ajouté avec succès.");
		} else {
			throw new BanqueException("Votre nouveau compte n'a pas été crée car vous en avez déjà 5.");
		}
	}
	
	public Compte getCompte(int num) {
		if (mapCompte.containsKey(num)) {
			return mapCompte.get(num);
		}
		return null;
	}

	@Override
	public String toString() {
		return "Client [nom=" + nom + ", prenom=" + prenom + ", age=" + age + ", numero=" + numero + ", mapCompte="
				+ mapCompte + "]";
	}
	
	/*
	public ArrayList<Compte> getListCompte() {
		return listCompte;
	}
	*/

	/*
	@Override
	public String toString() {
		return "Client [nom=" + nom + ", prenom=" + prenom + ", age=" + age + ", numero=" + numero + ", listCompte="
				+ listCompte + "]";
	}
	*/

	/*
	public void ajouterCompte(Compte c) throws BanqueException {
		Compte[] listeCompteArray = listCompte.toArray(new Compte[listCompte.size()]);
		boolean insert = false;
		for (int i = 0; i < listeCompteArray.length; i++) {
			if (insert == false && listeCompteArray[i] == null) {
				listeCompteArray[i] = c;
				insert = true;
			}
		}
		if (!insert) {
			throw new BanqueException("Votre nouveau compte n'a pas été crée car vous en avez déjà 5.");
		} else {
			System.out.println("Votre nouveau compte a été ajouté avec succès.");
		}
	}
	*/
	
	/*
	public Compte getCompte(int num) {
		Compte[] listeCompteArray = listCompte.toArray(new Compte[listCompte.size()]);
		for (int i = 0; i < listeCompteArray.length; i++) {
			if (listeCompteArray[i] != null && listeCompteArray[i].getNumero() == num) {
				return listeCompteArray[i];
			}
		}
		return null;
	}
	*/
}
