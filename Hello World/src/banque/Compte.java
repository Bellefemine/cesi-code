package banque;

public class Compte {
	private double solde;
	private int numero;
	
	public Compte(double solde, int numero) {
		this.solde = solde;
		this.numero = numero;
	}
	
	public void ajouter(double m) {
		if (m >= 0) {
			this.solde += m;
		}
	}
	
	public void retirer(double m) throws BanqueException{
		if (m >= 0) {
			this.solde -= m;
		}
	}

	public double getSolde() {
		return solde;
	}
	
	public void setSolde(double solde) {
		this.solde = solde;
	}

	public int getNumero() {
		return numero;
	}

	@Override
	public String toString() {
		return "Compte [solde=" + solde + ", numero=" + numero + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Compte other = (Compte) obj;
		if (numero != other.numero)
			return false;
		if (Double.doubleToLongBits(solde) != Double.doubleToLongBits(other.solde))
			return false;
		return true;
	}
}
