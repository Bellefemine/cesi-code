package banque;

public interface ICompteASeuil {
	public void setSeuil(double seuil);
	public double getSeuil();
	public void retirer(double montant) throws BanqueException;
}
