package banque;

public interface ICompteRemunere {
	public double calculerInterets();
	public void ajouterInterets();
	public void setTaux(double Taux);
	public double getTaux();
}
