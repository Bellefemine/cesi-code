package banque_multiheritage;

public class Compte implements ICompte {
	
	private double solde;
	private int numero;
	
	public Compte(double solde, int numero) {
		this.solde = solde;
		this.numero = numero;
	}

	@Override
	public double getSolde() {
		return solde;
	}

	@Override
	public void setSolde(double solde) {
		this.solde = solde;
	}

	@Override
	public int getNumero() {
		return numero;
	}

	@Override
	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Compte [solde=" + solde + ", numero=" + numero + "]";
	}

}
