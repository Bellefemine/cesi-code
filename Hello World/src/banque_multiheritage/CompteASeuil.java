package banque_multiheritage;

public class CompteASeuil extends Compte implements ICompteASeuil{

	private double seuil;
	
	public CompteASeuil(double solde, int numero, double seuil) {
		super(solde, numero);
		this.seuil = seuil;
	}

	@Override
	public double getSeuil() {
		return seuil;
	}

	@Override
	public void setSeuil(double seuil) {
		this.seuil = seuil;
	}

	@Override
	public String toString() {
		return "CompteASeuil [seuil=" + seuil + ", getSolde()=" + getSolde() + ", getNumero()=" + getNumero() + "]";
	}

}
