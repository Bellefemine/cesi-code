package banque_multiheritage;

public class CompteASeuilRemunere extends Compte implements ICompteASeuilRemunere {

	public double taux;
	public double seuil;
	
	public CompteASeuilRemunere(double solde, int numero, double taux, double seuil) {
		super(solde, numero);
		this.taux = taux;
		this.seuil = seuil;
	}

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		this.taux = taux;
	}

	public double getSeuil() {
		return seuil;
	}

	public void setSeuil(double seuil) {
		this.seuil = seuil;
	}

	@Override
	public String toString() {
		return "CompteASeuilRemunere [taux=" + taux + ", seuil=" + seuil + ", getSolde()=" + getSolde()
				+ ", getNumero()=" + getNumero() + "]";
	}

}
