package banque_multiheritage;

public class CompteRemunere extends Compte implements ICompteRemunere {
	
	private double taux;
	
	public CompteRemunere(double solde, int numero, double taux) {
		super(solde, numero);
		this.taux = taux;
	}

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		this.taux = taux;
	}

	@Override
	public String toString() {
		return "CompteRemunere [taux=" + taux + ", getSolde()=" + getSolde() + ", getNumero()=" + getNumero() + "]";
	}
}
