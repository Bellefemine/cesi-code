package banque_multiheritage;

public interface ICompte {
	
	public default void ajouter(double m) {
		if (m >= 0) {
			this.setSolde(getSolde() + m);
		}
	}
	
	public default void retirer(double m) throws BanqueException {
		if (m >= 0) {
			this.setSolde(getSolde() - m);
		}
	}

	public double getSolde();
	public void setSolde(double solde);
	public int getNumero();
	public void setNumero(int numero);
	public String toString();
	
}
