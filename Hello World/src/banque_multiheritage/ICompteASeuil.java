package banque_multiheritage;

public interface ICompteASeuil extends ICompte {

	public double getSeuil();
	public void setSeuil(double seuil);
	
	public default void retirer(double montant) throws BanqueException {
		if ((this.getSolde() - montant) > this.getSeuil()) {
			this.setSolde(this.getSolde() - montant);
		} else {
			throw new BanqueException("Le solde de votre compte ne peux pas passer sous le seuil de " + this.getSeuil() + ".");
		}
	}
	
	public String toString();
	
}
