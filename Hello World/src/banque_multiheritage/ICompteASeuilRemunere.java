package banque_multiheritage;

public interface ICompteASeuilRemunere extends ICompteRemunere, ICompteASeuil{
	public String toString();
}
