package banque_multiheritage;

public interface ICompteRemunere extends ICompte {
	public double getTaux();
	public void setTaux(double taux);
	public String toString();
	
	public default double calculerInterets() {
		return this.getTaux() * this.getSolde();
	}
	
	public default void ajouterInterets() {
		this.setSolde(this.getSolde() + this.calculerInterets());
	}
}
