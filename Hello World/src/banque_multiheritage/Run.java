package banque_multiheritage;

public class Run {

	public static void main(String[] args) {

		Compte c0 = new Compte(1000, 0);
		//System.out.println(c0);
		
		Compte c1 = new CompteRemunere(1000, 1, 0.1);
		//System.out.println(c1);
		((CompteRemunere) c1).ajouterInterets();
		//System.out.println(c1);
		
		Compte c2 = new CompteASeuil(1000, 2, 900);
		//System.out.println(c2);
		try {
			((CompteASeuil) c2).retirer(50);
			//System.out.println(c2);
			//((CompteASeuil) c2).retirer(100);
			//System.out.println(c2);
		} catch (BanqueException e) {
			e.printStackTrace();
		}
		
		Compte c3 = new CompteASeuilRemunere(1000, 3, 0.1, 950);
		//System.out.println(c3);
		try {
			((CompteASeuilRemunere) c3).retirer(100);
			//System.out.println(c3);
			
		} catch (BanqueException e) {
			e.printStackTrace();
		}

		((CompteASeuilRemunere) c3).ajouterInterets();
		//System.out.println(c3);
	}
}

