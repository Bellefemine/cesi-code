package io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class NameReader {
	
	private static ArrayList<String> readfile(String filename) throws IOException{
		ArrayList<String> lines = new ArrayList<String>();
		
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		
		String line = "";
		while ((line  = reader.readLine()) != null) {
			lines.add(line);
		}
		
	    reader.close();
		return lines;
	}
	
	public static ArrayList<String> readName() throws IOException {
		return readfile("src/io/nom.txt");
	}
	
	public static ArrayList<String> readFName() throws IOException {
		return readfile("src/io/prenom.txt");
	}
	
}
