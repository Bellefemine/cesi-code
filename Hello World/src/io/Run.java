package io;

import java.io.IOException;
import java.util.ArrayList;

public class Run {

	public static void main(String[] args) {
		try {
			ArrayList<String> name = NameReader.readName();
			ArrayList<String> fName = NameReader.readFName();
			
			//System.out.println(name);
			//System.out.println(fName);
			
			UserWriter.writeUsers("users.txt", name, fName, 50);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
