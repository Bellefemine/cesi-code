package io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;

public class UserWriter {
	
	public static void writeUsers(String filename, ArrayList<String> name, ArrayList<String> fName, int count) throws IOException {
		writeInFile(generateNames(name, fName, count), filename);
	}
	
	private static ArrayList<String> generateNames(ArrayList<String> name, ArrayList<String> fName, int count) throws IOException {
		ArrayList<String> names = new ArrayList<String>();
		
		Random rand = new Random();
		
		String selectedName = "";
		String selectedFName = "";
		char gender = 'M';
		
		for (int i = 0; i < count; i++) {
			selectedName = name.get(rand.nextInt(name.size()));
			selectedFName = fName.get(rand.nextInt(fName.size()));
			if (rand.nextBoolean() == true) {
				gender = 'F';
			} else {
				gender = 'M';
			}
			names.add(i + ";" + selectedFName + ";" + selectedName + ";" + gender);
		}
		
		return names;
	}
	
	private static void writeInFile(ArrayList<String> listNames, String filename) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File("src/io/" + filename), StandardCharsets.UTF_8));

		System.out.println(listNames);
		for (int i = 0; i < listNames.size(); i++) {
			writer.write(listNames.get(i));
			writer.newLine();
		}
		
		writer.close();
		
	}
}
