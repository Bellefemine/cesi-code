package point2D;

public class Point2D {
	
	private static int compteur = 1;

	private int x;
	private int y;
	
	public Point2D() {
		this.x = 0;
		this.y = 0;
		compteur += 1;
	}
	
	public Point2D(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void translate(int dX, int dY) {
		this.x += dX;
		this.y += dY;
	}
	
	public String toString() {
		return "[" + x + ", " + y +"]";
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public static int getCompteur() {
		return compteur;
	}

}
