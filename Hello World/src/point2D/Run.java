package point2D;

public class Run {

	public static void main(String[] args) {

		Point2D p = new Point2D(5, 2);
		System.out.println(p);
		p.translate(2, 8);
		System.out.println(p);
		System.out.println(p.getCompteur());
		
		Point2D p0 = new Point2D();
		System.out.println(p0);
		System.out.println(p.getCompteur());
		
		Point3D p3D = new Point3D();
		System.out.println(p3D);
		p3D.translate(2, 6, 4);
		System.out.println(p3D);
		
	}

}
