package jaxb;

@Xml
public class Adresse {
	
	private String pays;
	private String ville;
	private String rue;
	private int cp;
	private int num;
	
	public Adresse () {
	}

	public Adresse(String pays, String ville, String rue, int cp, int num) {
		super();
		this.pays = pays;
		this.ville = ville;
		this.rue = rue;
		this.cp = cp;
		this.num = num;
	}
	
	
}
