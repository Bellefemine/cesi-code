package com.example.myapplication.Content;

import java.io.Serializable;

public class Content implements Serializable {

    private String title;
    private String text;

    public Content(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Content{" +
                "title='" + title + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
