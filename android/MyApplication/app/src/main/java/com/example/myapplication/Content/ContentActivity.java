package com.example.myapplication.Content;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ContentActivity extends AppCompatActivity {

    ListView list;
    private ArrayList<Content> contentList = new ArrayList<>();
    private Activity context = this;
    private int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_content);
            getSupportActionBar().hide();
            ((TextView) findViewById(R.id.username)).setText(MainActivity.userRepository.readUser().getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        displayList(this, contentList);
    }

    private void displayList(Activity context, ArrayList<Content> listContent) {
        ContentAdapter adapter = new ContentAdapter(this, contentList);
        list = (ListView) findViewById(R.id.listView);
        list.setAdapter(adapter);
    }

    // Launch activity to add a content, waiting for response
    ActivityResultLauncher<Intent> launch = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        Content c = new Content(data.getExtras().get("title").toString(), data.getExtras().get("text").toString());
                        contentList.add(c);
                        //generateTextField();
                        displayList(context, contentList);
                    }
                }
            }
    );

    public void openDetailActivity(View view) {
        View parent = (View) view.getParent();
        ListView list = (ListView) parent.getParent();
        int index = list.getPositionForView(parent);
        Intent intent = new Intent(this, ContentDetailActivity.class);
        intent.putExtra("title", contentList.get(index).getTitle().toString());
        intent.putExtra("text", contentList.get(index).getText().toString());
        startActivity(intent);
    }

    /* Old Version of list
    public void generateTextField() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.listLayout);
        layout.removeAllViews();
        for (int i = 0; i < contentList.size(); i++) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            TextView title = new TextView(getApplicationContext());
            title.setText(contentList.get(i).getTitle());
            TextView text = new TextView(getApplicationContext());
            text.setText(contentList.get(i).getText());
            linearLayout.addView(title);
            linearLayout.addView(text);
            layout.addView(linearLayout);
        }
    }
     */

    public void openAddItem(View view) {
        Intent intent = new Intent(this, CustomAddActivity.class);
        launch.launch(intent);
    }
}
