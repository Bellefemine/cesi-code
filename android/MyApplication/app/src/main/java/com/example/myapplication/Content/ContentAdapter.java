package com.example.myapplication.Content;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;

import java.util.ArrayList;

public class ContentAdapter extends ArrayAdapter {
    private ArrayList<Content> listContent;
    private Activity context;

   public ContentAdapter(Activity context, ArrayList<Content> contentList) {
       super(context, R.layout.list_view_layout, contentList);
       this.listContent = contentList;
       this.context = context;
   }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_view_layout, null,true);

        TextView titleText = (TextView) rowView.findViewById(R.id.title);
        TextView subtitleText = (TextView) rowView.findViewById(R.id.text);

        titleText.setText(listContent.get(position).getTitle().toString());
        subtitleText.setText(listContent.get(position).getText().toString());

        return rowView;
    }
}
