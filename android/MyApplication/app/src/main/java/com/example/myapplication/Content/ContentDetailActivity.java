package com.example.myapplication.Content;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;

public class ContentDetailActivity extends AppCompatActivity {

    private String title;
    private String text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().hide();
        Intent intent = getIntent();
        title = intent.getExtras().get("title").toString();
        text = intent.getExtras().get("text").toString();
        ((TextView) findViewById(R.id.showTitle)).setText(title);
        ((TextView) findViewById(R.id.showDesc)).setText(text);
    }

    public void closeActivity(View view) {
        finish();
    }

}
