package com.example.myapplication.Content;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;

import java.io.ObjectOutputStream;
import java.io.Serializable;

public class CustomAddActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_custom);
            getSupportActionBar().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addItem(View view){
        String title = ((EditText) findViewById(R.id.TitleCustom)).getText().toString();
        String text = ((EditText) findViewById(R.id.TextCustom)).getText().toString();
        Intent intent = new Intent();
        intent.putExtra("title", title);
        intent.putExtra("text", text);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}