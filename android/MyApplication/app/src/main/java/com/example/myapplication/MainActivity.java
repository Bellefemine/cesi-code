package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.Content.ContentActivity;
import com.example.myapplication.Repository.UserRepository;
import com.example.myapplication.login.LoginActivity;
import com.example.myapplication.login.User;

public class MainActivity extends AppCompatActivity {

    private static User user = null;
    public static UserRepository userRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        this.userRepository = new UserRepository(MainActivity.this);
    }

    public void goToContent(View view) {
        try {
            Intent intent;
            if (user == null) {
                intent = new Intent(this, LoginActivity.class);
            } else {
                intent = new Intent(this, ContentActivity.class);
            }
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast toast = Toast.makeText(this, "Erreur de chargement de la page !", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public static User getUser() {
        return MainActivity.user;
    }

    public static void setUser(User user) {
        MainActivity.user = user;
    }
}