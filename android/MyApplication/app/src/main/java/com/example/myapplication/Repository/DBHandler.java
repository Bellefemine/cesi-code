package com.example.myapplication.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.Content.Content;

import java.io.File;

public class DBHandler extends SQLiteOpenHelper {

    public final String tableName = "Login";
    public final String nameCol = "username";
    public Context context;

    public DBHandler(Context context) {
        super(context, "DB_LOGIN", null, 1);
        this.context = context;
    }

    public DBHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            String query = "" +
                    "CREATE TABLE " + tableName +
                    "(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    nameCol + " TEXT" +
                    ");";
            sqLiteDatabase.execSQL(query);
        }  catch (Exception e) {
            e.printStackTrace();
            Toast toast = Toast.makeText(context, "Erreur à la création de la table !", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + tableName);
        onCreate(sqLiteDatabase);
    }
}
