package com.example.myapplication.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.example.myapplication.login.User;

public class UserRepository {

    private Context context;
    private DBHandler dbHandler;

    public UserRepository(Context context) {
        this.context = context;
        try {
            this.dbHandler = new DBHandler(context);
        } catch (Exception e) {
            e.printStackTrace();
            Toast toast = Toast.makeText(context, "Erreur à l'initialisation de la base !", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void addUser(String username) {
        try {
            SQLiteDatabase sql = dbHandler.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(dbHandler.nameCol, username);
            System.out.println(username);
            sql.insert(dbHandler.tableName, null, values);
            sql.close();
            Log.i("addUser", "Ajout d'un utilisateur en BDD: " + username);
        } catch (Exception e) {
            e.printStackTrace();
            Toast toast = Toast.makeText(context, "Erreur à l'ajout d'un utilisateur !", Toast.LENGTH_LONG);
            toast.show();
        }

    }

    public User readUser() {
        try {
            SQLiteDatabase sql = dbHandler.getReadableDatabase();
            Cursor cursor = sql.rawQuery("SELECT " + dbHandler.nameCol + " FROM " + dbHandler.tableName + ";", null);
            if (cursor.moveToFirst()) {
                Log.i("read", "Lecture d'un utilisateur dans la base de donnée: " + cursor.getString(0));
                return new User(cursor.getString(0), "hop");
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast toast = Toast.makeText(context, "Erreur à la lecture de l'utilisateur !", Toast.LENGTH_LONG);
            toast.show();
        }
        return null;
    }
}
