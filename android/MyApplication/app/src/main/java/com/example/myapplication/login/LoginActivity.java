package com.example.myapplication.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.Content.ContentActivity;
import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

public class LoginActivity extends AppCompatActivity {

    private User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
    }

    public void goToHome(View view) {
        finish();
    }

    public void cancelCreds(View view) {
        ((EditText) findViewById(R.id.nameInput)).setText("");
        ((EditText) findViewById(R.id.pwdInput)).setText("");
    }

    public void checkCreds(View view) {
        String name = ((EditText) findViewById(R.id.nameInput)).getText().toString();
        String pwd = ((EditText) findViewById(R.id.pwdInput)).getText().toString();
        this.user = new User(name, pwd);
        if (user.checkCreds()) {
            // GRANTED
            MainActivity.setUser(this.user);
            try {
                MainActivity.userRepository.addUser(user.getName());
                Intent intent = new Intent(this, ContentActivity.class);
                startActivity(intent);
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            // REJECTED
            Toast toast = Toast.makeText(this, "Mot de passe ou username incorrect !", Toast.LENGTH_LONG);
            toast.show();
        }
    }
}