package com.example.myapplication.login;

import java.util.Objects;

public class User {

    private String name;
    private String pwd;

    private static final String goodName = "Louis";
    private static final String goodPwd = "hop";

    public User(String name, String pwd) {
        this.name = name;
        this.pwd = pwd;
    }

    public User()  {
        this.name = "Louis";
        this.pwd = "hop";
    }

    public boolean checkCreds() {
        if (name.equals(goodName) && pwd.equals(goodPwd)) {
            return true;
        } else {
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) && Objects.equals(pwd, user.pwd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, pwd);
    }
}
