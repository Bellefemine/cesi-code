package banque;

public class CompteASeuil extends Compte implements ICompteASeuil{
	
	private double seuil;

	public CompteASeuil(double solde, int numero, double seuil) {
		super(solde, numero);
		this.seuil = seuil;
	}

	public double getSeuil() {
		return seuil;
	}

	public void setSeuil(double seuil) {
		this.seuil = seuil;
	}

	@Override
	public String toString() {
		return "CompteASeuil [seuil=" + seuil + ", getSolde()=" + getSolde() + ", getNumero()=" + getNumero()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}
	
	@Override
	public void retirer(double montant) throws BanqueException{
		if ((super.getSolde() - montant) > seuil) {
			super.setSolde(super.getSolde() - montant);
		} else {
			throw new BanqueException("Le solde de votre compte ne pas pas passer sous le seuil fixé de " + this.getSeuil() + "€.");
		}
	}

}
