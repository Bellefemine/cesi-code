package banque;

public class CompteASeuilRemunere extends CompteRemunere implements ICompteASeuil{
	
	private double seuil;

	public CompteASeuilRemunere(double solde, int numero, double taux, double seuil) {
		super(solde, numero, taux);
		this.seuil = seuil;
	}

	public void setSeuil(double seuil) {
		this.seuil = seuil;
	}

	public double getSeuil() {
		return this.seuil;
	}
	
	@Override
	public void retirer(double montant) throws BanqueException{
		if ((super.getSolde() - montant) > seuil) {
			super.setSolde(super.getSolde() - montant);
		} else {
			throw new BanqueException("Le solde de votre compte ne pas pas passer sous le seuil fixé de " + this.getSeuil() + "€.");
		}
	}

	@Override
	public String toString() {
		return "CompteASeuilRemunere [seuil=" + seuil + ", getTaux()=" + getTaux() + ", getSolde()=" + getSolde() + "]";
	}
	
	

}
