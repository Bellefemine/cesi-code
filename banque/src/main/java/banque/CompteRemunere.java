package banque;

public class CompteRemunere extends Compte implements ICompteRemunere{
	
	private double taux;

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		this.taux = taux;
	}

	public CompteRemunere(double solde, int numero, double taux) {
		super(solde, numero);
		if (taux > 0 && taux < 1) {
			this.taux = taux;
		} else {
			this.taux = 0;
		}
	}

	public double calculerInterets() {
		return taux * getSolde();
	}
	
	public void ajouterInterets() {
		super.setSolde(getSolde() + calculerInterets());
	}

	@Override
	public String toString() {
		return "CompteRemunere [taux=" + taux + ", getSolde()=" + getSolde() + ", getNumero()=" + getNumero() + "]";
	}

}
