package banque;

import java.sql.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

public class DB {
	
	private String dbDriver = "com.mysql.jdbc.Driver";
	private String dbUrl = "jdbc:mysql://localhost/banque?useSSL=false";
	private final String dbUser = "root";
	private final String dbPwd = "password";
	
	private Connection connection = null;
	private Statement request = null;
	private ResultSet results = null;
	
	public DB() {
		try {
			Class.forName(dbDriver);
			System.out.println("DB driver loaded successfully !");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void connect() {
		if (this.connection == null) {
			try {
				this.connection = DriverManager.getConnection(dbUrl, dbUser, dbPwd);
				System.out.println("Connection opened successfully !");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void close() {
		try {
			if (!this.connection.isClosed()) {
				this.connection.close();
				System.out.println("Connection closed successfully !");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> requestName() {
		ArrayList<String> listeUsers = new ArrayList<String>();
		try {
			request = this.connection.createStatement();
			results = request.executeQuery("SELECT nom, prenom FROM utilisateur;");
			
			while(results.next()) {
				String nom = results.getString("nom");
				String prenom = results.getString("prenom");
				listeUsers.add(prenom + " " + nom);
			}
			
			results.close();
			request.close();
			
			return listeUsers;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<Client> recupererClients() {
		ArrayList<Client> list = new ArrayList<Client>();
		try {
			request = this.connection.createStatement();
			results = request.executeQuery("SELECT nom, prenom, dateDeNaissance as age, id FROM utilisateur;");
		
		while(results.next()) {
			String nom = results.getString("nom");
			String prenom = results.getString("prenom");
			String age = results.getString("age");
			String numero = results.getString("id");
			
			list.add(new Client(nom, prenom, calculateAge(age), Integer.parseInt(numero)));
		}
		
		results.close();
		request.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	private int calculateAge(String year) {
		if (year == null) {
			return -1;
		}
		LocalDate birth = LocalDate.parse(year);
		LocalDate now = LocalDate.now();
		Period p = Period.between(birth, now);
		return p.getYears();
	}
	
	public ArrayList<Compte> recupererCompte(Client client) {
		ArrayList<Compte> list = new ArrayList<Compte>();
		
		try {
			request = this.connection.createStatement();
			results = request.executeQuery(generateCompteQuery(client));
		
		while(results.next()) {
			String solde = results.getString("solde");
			String numero = results.getString("id");
			String decouvert = results.getString("decouvert");
			String taux = results.getString("taux");
			
			list.add(generateCompteWithType(solde, numero, decouvert, taux));
		}
		
		results.close();
		request.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public String generateCompteQuery(Client client) {
		return "SELECT * FROM compte WHERE utilisateurId = '" + client.getNumero() +"';";
	}
	
	public Compte generateCompteWithType(String solde, String numero, String decouvert, String taux) {
		if (taux != null && !taux.isEmpty()) {
			if (decouvert != null && !decouvert.isEmpty()){
				return new CompteASeuilRemunere(Double.parseDouble(solde), Integer.parseInt(numero), Double.parseDouble(taux), Double.parseDouble(decouvert));
			} else {
				return new CompteRemunere(Double.parseDouble(solde), Integer.parseInt(numero), Double.parseDouble(taux));
			}
		} else if (decouvert != null && !decouvert.isEmpty()){
			if (taux != null && !taux.isEmpty()) {
				return new CompteASeuilRemunere(Double.parseDouble(solde), Integer.parseInt(numero), Double.parseDouble(taux), Double.parseDouble(decouvert));
			} else {
				return new CompteASeuil(Double.parseDouble(solde), Integer.parseInt(numero), Double.parseDouble(decouvert));
			}
		} else {
			return new Compte(Double.parseDouble(solde), Integer.parseInt(numero));
		}
	}
	
	public ArrayList<Operation> recupererOperation(Compte compte) {
		ArrayList<Operation> liste = new ArrayList<Operation>();
		
		try {
			request = this.connection.createStatement();
			results = request.executeQuery(generateOpeQuery(compte));
		
		while(results.next()) {
			String numero = results.getString("id");
			String libelle = results.getString("libelle");
			String montant = results.getString("montant");
			Date date = results.getDate("date");
			liste.add(new Operation(Integer.parseInt(numero), libelle, Double.parseDouble(montant), date));
		}
		
		results.close();
		request.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	
	public String generateOpeQuery(Compte compte) {
		return "SELECT id, libelle, montant, date FROM operation WHERE compteId = '" + compte.getNumero() + "';";
	}

}
