package banque;

// import java.util.ArrayList;

public class Run {

	public static void main(String[] args) {
		
		DB db = new DB();
		db.connect();
		System.out.println(db.requestName());
		System.out.println(db.recupererClients());
		System.out.println(db.recupererCompte(db.recupererClients().get(0)));
		System.out.println(db.recupererOperation(db.recupererCompte(db.recupererClients().get(0)).get(0)));
		db.close();
		
	}

}

/*
Client c = new Client("Louis", "Bellefemine", 21, 1);
System.out.println(c.toString());

try {
	c.ajouterCompte(new Compte(10, 0));
	c.ajouterCompte(new Compte(10, 1));
	c.ajouterCompte(new Compte(10, 2));
	c.ajouterCompte(new Compte(10, 3));
	c.ajouterCompte(new Compte(10, 4));
	c.ajouterCompte(new Compte(10, 5));
} catch (BanqueException e) {
	System.out.println(e);
}

System.out.println(c.getListCompte());

ArrayList<Compte> listCompte = new ArrayList<Compte>();
listCompte.add(new CompteASeuilRemunere(1000, 0, 0.1, 1000));
listCompte.add(new CompteASeuilRemunere(50, 1, 0.25, 25));
listCompte.add(new CompteASeuilRemunere(5000, 0, 0.05, 2000));

for (Compte compte : listCompte) {
	System.out.println(compte.toString());
}

try {
	for (Compte compte : listCompte) {
		compte.retirer(50);
	}
} catch (BanqueException e) {
	System.out.println(e);
}


for (Compte compte : listCompte) {
	if (compte instanceof CompteRemunere) {
		((CompteASeuilRemunere) compte).ajouterInterets();
	}
}

for (Compte compte : listCompte) {
	System.out.println(compte.toString());
}

*/