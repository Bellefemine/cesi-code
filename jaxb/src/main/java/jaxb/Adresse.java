package jaxb;

import javax.xml.bind.annotation.*;

@XmlRootElement(name= "adresse")
@XmlAccessorType(XmlAccessType.FIELD)
public class Adresse {
	
	@XmlAttribute(name = "pays")
	private String pays;
	@XmlAttribute(name = "ville")
	private String ville;
	@XmlElement(name = "rue")
	private String rue;
	@XmlAttribute(name = "cp")
	private int cp;
	@XmlAttribute(name = "num")
	private int num;
	
	public Adresse() {}

	public Adresse(String pays, String ville, String rue, int cp, int num) {
		super();
		this.pays = pays;
		this.ville = ville;
		this.rue = rue;
		this.cp = cp;
		this.num = num;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public int getCp() {
		return cp;
	}

	public void setCp(int cp) {
		this.cp = cp;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	@Override
	public String toString() {
		return "Adresse [pays=" + pays + ", ville=" + ville + ", rue=" + rue + ", cp=" + cp + ", num=" + num + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cp;
		result = prime * result + num;
		result = prime * result + ((pays == null) ? 0 : pays.hashCode());
		result = prime * result + ((rue == null) ? 0 : rue.hashCode());
		result = prime * result + ((ville == null) ? 0 : ville.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Adresse other = (Adresse) obj;
		if (cp != other.cp)
			return false;
		if (num != other.num)
			return false;
		if (pays == null) {
			if (other.pays != null)
				return false;
		} else if (!pays.equals(other.pays))
			return false;
		if (rue == null) {
			if (other.rue != null)
				return false;
		} else if (!rue.equals(other.rue))
			return false;
		if (ville == null) {
			if (other.ville != null)
				return false;
		} else if (!ville.equals(other.ville))
			return false;
		return true;
	}
	
}
