package jaxb;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAdapter extends javax.xml.bind.annotation.adapters.XmlAdapter<String, Date>{
	
	@Override
	public Date unmarshal(String v) throws Exception {
		return new SimpleDateFormat().parse(v);
	}

	@Override
	public String marshal(Date v) throws Exception {
		return v.toString();
	}

}
