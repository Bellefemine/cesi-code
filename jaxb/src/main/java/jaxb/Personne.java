package jaxb;

import java.time.LocalDate;
import java.util.ArrayList;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name= "personne")
@XmlAccessorType(XmlAccessType.FIELD)
public class Personne {
	
	@XmlElement(name = "nom")
	private String nom;
	@XmlElement(name = "prenom")
	private String prenom;
	@XmlElement(name = "date")
	private LocalDate date;
	
	@XmlElementWrapper(name = "adresses")
	@XmlElement(name = "adresse")
	private ArrayList<Adresse> listeAdresse = new ArrayList<Adresse>();
	
	public Personne(String nom, String prenom, LocalDate date) {
		this.nom = nom;
		this.prenom = prenom;
		this.date = date;
	}
	
	public Personne(String nom, String prenom, LocalDate date, ArrayList<Adresse> listeAdresse) {
		this.nom = nom;
		this.prenom = prenom;
		this.date = date;
		this.listeAdresse = listeAdresse;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	//@XmlJavaTypeAdapter(value = DateAdapter)
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public ArrayList<Adresse> getListeAdresse() {
		return listeAdresse;
	}

	public void setListeAdresse(ArrayList<Adresse> listeAdresse) {
		this.listeAdresse = listeAdresse;
	}

	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", date=" + date + ", listeAdresse=" + listeAdresse
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((listeAdresse == null) ? 0 : listeAdresse.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personne other = (Personne) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (listeAdresse == null) {
			if (other.listeAdresse != null)
				return false;
		} else if (!listeAdresse.equals(other.listeAdresse))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}
}
